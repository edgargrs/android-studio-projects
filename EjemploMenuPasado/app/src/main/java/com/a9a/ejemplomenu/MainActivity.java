package com.a9a.ejemplomenu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.content.Intent;
import android.net.Uri;


public class MainActivity extends AppCompatActivity {
    Button btcontextual, btpopup;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btcontextual=(Button)findViewById((R.id.btMenuComtextual));
        registerForContextMenu(btcontextual);
        btpopup=(Button)findViewById(R.id.btMenuPopup);
        btpopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu menuPopUp=new PopupMenu(MainActivity.this, v);
                menuPopUp.setOnMenuItemClickListener(eventoPopUp);
                MenuInflater inflater=menuPopUp.getMenuInflater();
                inflater.inflate(R.menu.menu_principal,menuPopUp.getMenu());
                menuPopUp.show();
            }
        });
    }





    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_principal,menu);
        menu.add(menu.NONE,100,menu.NONE,"Cerrar");
    }



    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_principal,menu);
        menu.add(menu.NONE,100,menu.NONE,"Cerrar");
        return true;
    }

    private PopupMenu.OnMenuItemClickListener eventoPopUp=new PopupMenu.OnMenuItemClickListener() {
        public boolean onMenuItemClick(MenuItem item) {
            int id = item.getItemId();
            if (id == R.id.contactos) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("content://contacts/people/"));
                startActivity(intent);
                return true;
            } else if (id == R.id.llamar) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:4431379735"));
                startActivity(intent);
                return true;
            } else if (id == R.id.ver_mapa) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:19.726794,-101.16199979"));
                startActivity(intent);
                return true;
            } else if (id == R.id.navegador) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://google.com.mx"));
                startActivity(intent);
                return true;
            } else if (id == 100) {
                finish();
            }
            return false;
        }
    };

    public boolean onContextItemSelected(MenuItem item){
        int id=item.getItemId();
        if (id==R.id.contactos){
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("content://contacts/people/"));
            startActivity(intent);
            return true;
        }else if(id==R.id.llamar){
            Intent intent= new Intent(Intent.ACTION_DIAL, Uri.parse("tel:4431379735"));
            startActivity(intent);
            return true;
        }else if(id==R.id.ver_mapa) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:19.726794,-101.16199979"));
            startActivity(intent);
            return true;
        }else if(id==R.id.navegador) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://google.com.mx"));
            startActivity(intent);
            return true;
        }else if (id==100){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onOptionsItemSelected(MenuItem item){
        int id=item.getItemId();
        if (id==R.id.contactos){
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("content://contacts/people/"));
            startActivity(intent);
            return true;
        }else if(id==R.id.llamar){
            Intent intent= new Intent(Intent.ACTION_DIAL, Uri.parse("tel:4431379735"));
            startActivity(intent);
            return true;
        }else if(id==R.id.ver_mapa) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:19.726794,-101.16199979"));
            startActivity(intent);
            return true;
        }else if(id==R.id.navegador) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://google.com.mx"));
            startActivity(intent);
            return true;
        }else if (id==100){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
