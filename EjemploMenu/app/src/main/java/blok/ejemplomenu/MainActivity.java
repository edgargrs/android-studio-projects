package blok.ejemplomenu;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.view.ContextMenu;
import android.content.Intent;
import android.net.Uri;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.view.MenuInflater;

public class MainActivity extends AppCompatActivity {

    Button btnCont, btnPop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnCont = ((Button)findViewById(R.id.btnMenuContext));
        registerForContextMenu(btnCont);

        btnPop = ( (Button) findViewById(R.id.btnPopUp));
        btnPop.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                PopupMenu menuPopUp = new PopupMenu(MainActivity.this, v);
                menuPopUp.setOnMenuItemClickListener(eventoPopUp);
                MenuInflater inflater=menuPopUp.getMenuInflater();
                inflater.inflate(R.menu.menu_principal,menuPopUp.getMenu());
                menuPopUp.show();
            }
        } );

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_principal, menu);
        menu.add(menu.NONE, 100, menu.NONE, "Cerrar");
        return true;
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_principal,menu);
        menu.add(menu.NONE,100,menu.NONE,"Cerrar");
    }

    private PopupMenu.OnMenuItemClickListener eventoPopUp=new PopupMenu.OnMenuItemClickListener() {
        public boolean onMenuItemClick(MenuItem item) {
            int id = item.getItemId();
            if (id == R.id.contactos) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("content://contacts/people/"));
                startActivity(intent);
                return true;
            } else if (id == R.id.llamar) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:4431379735"));
                startActivity(intent);
                return true;
            } else if (id == R.id.ver_mapa) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:19.726794,-101.16199979"));
                startActivity(intent);
                return true;
            } else if (id == R.id.navegar) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://google.com.mx"));
                startActivity(intent);
                return true;
            } else if (id == 100) {
                finish();
            }
            return false;
        }
    };

    public boolean onContextItemSelected(MenuItem item){
        int id=item.getItemId();
        if (id==R.id.contactos){
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("content://contacts/people/"));
            startActivity(intent);
            return true;
        }else if(id==R.id.llamar){
            Intent intent= new Intent(Intent.ACTION_DIAL, Uri.parse("tel:4431379735"));
            startActivity(intent);
            return true;
        }else if(id==R.id.ver_mapa) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:19.726794,-101.16199979"));
            startActivity(intent);
            return true;
        }else if(id==R.id.navegar) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://google.com.mx"));
            startActivity(intent);
            return true;
        }else if (id==100){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if(id == R.id.contactos){
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("content://contacts/people/"));
            startActivity(intent);
            return true;
        }else if(id == R.id.llamar){
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:3278049"));
            startActivity(intent);
            return true;
        }else if(id == R.id.ver_mapa){
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:19.726794,-101.161.99979"));
            startActivity(intent);
            return true;
        }else if( id == R.id.navegar){
            Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse("http://google.com.mx"));
            startActivity(intent);
            return true;
        }else if(id == 100){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
