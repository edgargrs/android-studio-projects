package com.a9a.loogin;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by gabo on 25/08/2017.
 */

public class WSlogin {
    //Namespace de nuestro Web Service
    private static String NAMESPACE = "http://sw/";
    //Webservice URL
    private static String URL = "http://192.168.208.209:8080/login/login?WSDL";
    //SOAP Action URI  Namespace + Web method name
    private static String SOAP_ACTION = "http://sw/";
    ///*****************************************METODO PARA LLAMAR A MÉTODO con un parámetroen el WEBSERVICE
    public static boolean LlamadaLogin(String userName,String passWord, String webMethName) {
// Por default dejamos a la variable, que esperamos como resultado de este método, como false
        boolean loginStatus = false;
// creamos la solicitud de conexion al web service
        SoapObject request = new SoapObject(NAMESPACE, webMethName);
// Creamos 2 variables de tipo PropetyInfo para lo parámetros que requerimos enel método de login en elWebService.
        PropertyInfo parametroUsr = new PropertyInfo();
        PropertyInfo parametroClave = new PropertyInfo();
//------Parametro Usuario
        parametroUsr.setName("usuario");
// Asignamos a la primer variable el nombre del primer parámetro que espera el WebService.
        parametroUsr.setValue(userName);
//Asignamos a esta variable el valor que pasemos como parametro en la llamada a éste método
        parametroUsr.setType(String.class);
//Asignamos el tipo de dato de la variable
        request.addProperty(parametroUsr);
//Hacemos el request de esta variable.
//------Parametro clave (password)
        parametroClave.setName("clave");
        parametroClave.setValue(passWord);
        parametroClave.setType(String.class);
        request.addProperty(parametroClave);
// Creación del envio del ws a traves de una instancia a la clase SoapSerializationEnvelope.
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        try
        {
//Iniciamos la excepcion para oder enviar la petición al Web Service.
            androidHttpTransport.call(SOAP_ACTION+webMethName, envelope);
//recibimos la respuesta del WebService en la variable loginStatus
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            loginStatus = Boolean.parseBoolean(response.toString());
        }
        catch
                (Exception e) {
//Si existe algun error en el envio, la variable errored que se creo en el metodo LlamadaLogin sera verdadera
            Login.errored = true;
            e.printStackTrace();
        }
//Regresamos como resultado de este método la variable loginStatus.
        return loginStatus;
    }

}
