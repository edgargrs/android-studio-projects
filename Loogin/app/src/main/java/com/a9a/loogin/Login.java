package com.a9a.loogin;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class Login extends AppCompatActivity {
    static boolean errored = false;
    TextView statusTV;
    ProgressBar webservicePG;
    String user,pass;
    boolean loginStatus;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }




    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu,menu);
        menu.add(menu.NONE,100,menu.NONE,"Cerrar");
        return true;
    }


    public boolean onOptionsItemSelected(MenuItem item){
        int id=item.getItemId();
        if(id==R.id.tec){
            LayoutInflater inflater = getLayoutInflater();
            View alertLayout = inflater.inflate(R.layout.layout_custom_dialog, null);
            final EditText etUsername = (EditText) alertLayout.findViewById(R.id.et_username);
            final EditText etPassword = (EditText) alertLayout.findViewById(R.id.et_password);
            final CheckBox cbShowPassword = (CheckBox) alertLayout.findViewById(R.id.cb_show_password);

            cbShowPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        // to encode password in dots
                        etPassword.setTransformationMethod(null);
                    } else {
                        // to display the password in normal text
                        etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    }
                }
            });

            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Login");
            alert.setIcon(R.mipmap.icon);
            // this is set the view from XML inside AlertDialog
            alert.setView(alertLayout);
            // disallow cancel of AlertDialog on click of back button and outside touch
            alert.setCancelable(false);
            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(getBaseContext(), "Cancel clicked", Toast.LENGTH_SHORT).show();
                }
            });

            alert.setPositiveButton("Login", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (etUsername.getText().length() != 0 && etUsername.getText().toString() != "") {
                        if (etPassword.getText().length() != 0 && etPassword.getText().toString() != ""){
                            user = etUsername.getText().toString();
                            pass = etPassword.getText().toString();
                            statusTV.setText("");
//Creamos un objeto de la clase AsyncCallWS para poder llamar al método.
                            AsyncCallWS task = new AsyncCallWS();
//Lllamada al método de execute de la clase AsyncCallWS
                            task.execute();
                        }
//Si el campo de la clase es vacio....
                        else
                        {
                            statusTV.setText("Favor de ingresar su clave");
                        }
//Si el campo del usuario es vacio....
                    }
                    else
                    {

                        statusTV.setText("Favor de ingresar su Usuario");
                    }

                    Toast.makeText(getBaseContext(), "Username: " + user + " Password: " + pass, Toast.LENGTH_SHORT).show();
                }
            });
            AlertDialog dialog = alert.create();
            dialog.show();


//.---------Obtenemos los id de los campos de texto, botón, barra  y de las etiquetas.
//------------------------------------------------------------------------------------

            statusTV = (TextView) findViewById(R.id.tv_result);

            webservicePG = (ProgressBar) findViewById(R.id.progressBar1);
// /

            return true;
        }else if (id==100){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    private class AsyncCallWS extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... params) {
//Lamamos al método CallLogin que definimos en la clase webservice, pasando como parametros los campos de texto
// y el nombre del método que llamaremos del webService.
            loginStatus = WSlogin.LlamadaLogin(user, pass, "login");
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
//Ocultar la barra loading
            webservicePG.setVisibility(View.INVISIBLE);
//creamos el Intent que llamara la siguiente activity, en este caso llamará a una activity llamada Menu
            Intent ActivitySiguiente = new Intent(Login.this,Menuu.class);
//si ocurre un error
            if (!errored){
//validamos el resultado del  método
                if (loginStatus){
//Si el resultado del metodo "LlamadaLogin" es verdadero, se cargará la activity definida en la variable ActivitySiguiente

                    startActivity(ActivitySiguiente);
                }
                else
                {
//Si el resultado del método es falso quiere decir que los datos del usuario son incorrectos.
                    statusTV.setText("DATOS INCORRECTOS");
                }
//Si la falla esta en la llamada al nuestro WebService:
            }
            else
            {
                statusTV.setText("Hay un error al llamar al Ws  :(");
            }
//La variable errored de nuevo la ponemos en false para indicar la falla
            errored = false;
        }
        @Override
//Mostramos la barra de loading mientras recibimos una respuesta.
        protected void onPreExecute() {
            webservicePG.setVisibility(View.VISIBLE);
        }
        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }



}
