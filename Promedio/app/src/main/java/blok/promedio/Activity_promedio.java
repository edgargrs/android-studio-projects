package blok.promedio;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Activity_promedio extends AppCompatActivity {

    TextView msj; //Objeto que manipulará la etiqueta declarada en el resultado.xml

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promedio);

        //Crea el objeto que se encarga de recibir las variables enviadas desde otra clase
        Bundle bundle = getIntent().getExtras();

        msj = (TextView) findViewById(R.id.tvResultado);
        msj.setText(bundle.getString("cadena"));
    }
}
