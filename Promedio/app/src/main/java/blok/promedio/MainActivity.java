package blok.promedio;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.view.View;
import android.content.Intent;

public class MainActivity extends AppCompatActivity {

    Button Guardar;
    EditText nombre, cal1, cal2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); //Layout que será levantado al inicio

        //Se inicializa el objeto, igualando con el elemento creado en el Layout en este caso el XML
        //contiene un botón nombre

        Guardar = (Button) findViewById(R.id.btGuardar);
        Guardar.setOnClickListener(new View.OnClickListener(){
            //Se agrega el listener al botón para activar su funcionamiento
            public void onClick(View v){
                //Al dar click en el botón se activa este metodo
                guardar();
            }
        });
    }

    public void guardar(){
        //Se inicializa los objetos restantes
        nombre = (EditText) findViewById(R.id.etAlumno);
        cal1 = (EditText) findViewById(R.id.etCal1);
        cal2 = (EditText) findViewById(R.id.etCal2);

        String alumno;
        double calif1, calif2, result;

        //GetText recupera la información de los EditText
        alumno = nombre.getText().toString();

        calif1 = Double.parseDouble(cal1.getText().toString());
        calif2 = Double.parseDouble(cal2.getText().toString());

        result = calif1 + calif2;

        String resultado = "Alumno : " + alumno + " \nCalificación: " + result;

        //Se crea un objeto de tipo intent que se encarga de activar el segundo layout.
        Intent i = new Intent(this, Activity_promedio.class);
        i.putExtra("cadena", resultado); //Envia una variable de nombre cadena que contiene el
        //resultado del calculo del promedio, promedio y es enviada a la clase resultado.java
        startActivity(i);
    }
}
