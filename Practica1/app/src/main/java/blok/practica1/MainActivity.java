package blok.practica1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.ArrayAdapter;
import java.util.Hashtable;
import java.util.ArrayList;
import android.content.Intent;
import android.net.Uri;
import android.view.Menu;
import android.view.MenuItem;


public class MainActivity extends AppCompatActivity {

    Button Guardar;
    EditText cliente;
    Spinner ncuenta;
    int saldoInicial;
    String[] lncuenta;

    //Hashtable<String, String> cuenta2743 = new Hashtable<String, String>();
    //Hashtable<String, String> cuenta23428 = new Hashtable<String, String>();

    ArrayList<String> cuenta2743 = new ArrayList<String>();
    ArrayList<String> cuenta3428 = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lncuenta = new String[] { "2743", "3428" };
        Spinner s = (Spinner) findViewById(R.id.spNcuenta);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, lncuenta);
        s.setAdapter(adapter);

        Guardar = (Button) findViewById(R.id.btnEnviar);
        Guardar.setOnClickListener(new View.OnClickListener(){
            //Se agrega el listener al botón para activar su funcionamiento
            public void onClick(View v){
                enviar();
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_principal, menu);
        menu.add(menu.NONE, 100, menu.NONE, "Cerrar");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if( id == R.id.navegar){
            Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse("http://google.com.mx"));
            startActivity(intent);
            return true;
        }else if(id == 100){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void enviar(){
        cliente = ((EditText) findViewById(R.id.etCliente));
        ncuenta = (Spinner) findViewById(R.id.spNcuenta);
        String cli, ncu;
        cli = cliente.getText().toString();
        ncu = ncuenta.getSelectedItem().toString();

        Intent i = new Intent(this, Activity_Deposito.class);

        if(ncu.equals("2743")){
            saldoInicial = 5000;
            cuenta2743.add(0, cli);
            cuenta2743.add(1, ncu);
            cuenta2743.add(2, Integer.toString(saldoInicial));
            i.putExtra("cuenta", cuenta2743);
        }else if(ncu.equals("3428")){
            saldoInicial = 7800;
            cuenta3428.add(0,  cli);
            cuenta3428.add(1, ncu);
            cuenta3428.add(2, Integer.toString(saldoInicial));
            i.putExtra("cuenta", cuenta3428);
        }else{
            saldoInicial = 0;
        }
        //i.putExtra("cliente", cli);
        //i.putExtra("cuenta", ncu);
        //i.putExtra("saldoi", saldoInicial);
        startActivity(i);
    }
}
