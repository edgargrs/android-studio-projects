package blok.practica1;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class Activity_Result extends AppCompatActivity {

    TextView tvNombre, tvCuenta, tvSaldoIni, tvDeposito, tvComision, tvSaldo;
    Button btnInicio;
    ArrayList<String> cuenta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__result);

        btnInicio = (Button) findViewById(R.id.btnInicio);
        btnInicio.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                inicio();
            }
        });

        //Bundle bundle = getIntent().getExtras();
        cuenta = (ArrayList<String>) getIntent().getSerializableExtra("cuenta");

        tvNombre = (TextView) findViewById(R.id.tvRNombre);
        tvNombre.setText(cuenta.get(0));
        //tvNombre.setText(bundle.getString("cliente"));

        tvCuenta = (TextView) findViewById(R.id.tvRCuenta);
        tvCuenta.setText(cuenta.get(1));
        //tvCuenta.setText(bundle.getString("cuenta"));

        tvSaldoIni = (TextView) findViewById(R.id.tvRSaldoI);
        tvSaldoIni.setText(cuenta.get(2));
        //tvSaldoIni.setText(bundle.getInt("saldoi")+"");

        tvDeposito = (TextView) findViewById(R.id.tvRDep);
        tvDeposito.setText(cuenta.get(3));
        //tvDeposito.setText(bundle.getDouble("montoDep")+"");

        tvComision = (TextView) findViewById(R.id.tvRCom);
        tvComision.setText(cuenta.get(4));
        //tvComision.setText(bundle.getDouble("comision")+"");

        tvSaldo = (TextView) findViewById(R.id.tvRSal);
        tvSaldo.setText(cuenta.get(5));
        //tvSaldo.setText(bundle.getDouble("saldoF")+"");

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_principal, menu);
        menu.add(menu.NONE, 100, menu.NONE, "Cerrar");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if( id == R.id.navegar){
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://google.com.mx"));
            startActivity(intent);
            return true;
        }else if(id == 100){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void inicio(){
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }
}
