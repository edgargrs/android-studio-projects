package blok.practica1;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import java.util.ArrayList;

import org.w3c.dom.Text;

import java.util.Hashtable;

public class Activity_Deposito extends AppCompatActivity {

    TextView nombre, monto, ncuenta;
    String nomClie, numcuenta;
    Double montDep, comision, saldoFinal = 0.0;
    int saldoi, operacion;
    Button Ejecutar;
    ArrayList<String> cuenta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__deposito);

        Ejecutar = (Button) findViewById(R.id.btnEjecutar);
        Ejecutar.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                ejecutar();
            }
        });

        //Bundle bundle = getIntent().getExtras();
        //nomClie = bundle.getString("cliente");
        //numcuenta = bundle.getString("cuenta");
        //saldoi = bundle.getInt("saldoi");
        //operacion = Integer.parseInt(bundle.getString("operacion"));

        cuenta = (ArrayList<String>) getIntent().getSerializableExtra("cuenta");


        nombre = (TextView) findViewById(R.id.tvNombreCliente);
        nombre.setText(cuenta.get(0));
        //nombre.setText(nomClie);
        ncuenta = (TextView) findViewById(R.id.tvNcuenta1);
        ncuenta.setText(cuenta.get(1));
        //ncuenta.setText(numcuenta);
        saldoFinal += Integer.parseInt(cuenta.get(2));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_principal, menu);
        menu.add(menu.NONE, 100, menu.NONE, "Cerrar");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if( id == R.id.navegar){
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://google.com.mx"));
            startActivity(intent);
            return true;
        }else if(id == 100){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void ejecutar(){
        monto = (TextView) findViewById(R.id.etMontoD);
        montDep = Double.parseDouble(monto.getText().toString());
        operacion = montDep.intValue() / 10000;
        if(operacion >= 1){
            comision = montDep * (operacion * 0.025);
            montDep -= comision;
            saldoFinal += montDep;
        }else{
            saldoFinal += montDep;
            comision = 0.0;
        }
        cuenta.add(3, Double.toString(montDep));
        cuenta.add(4, Double.toString(comision));
        cuenta.add(5, Double.toString(saldoFinal));

        Intent i = new Intent(this, Activity_Result.class);
        i.putExtra("cuenta", cuenta);
        startActivity(i);
    }
}
