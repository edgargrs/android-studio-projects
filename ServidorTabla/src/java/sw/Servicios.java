/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sw;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Alumno
 */
@WebService(serviceName = "Servicios")
public class Servicios {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "tabla")
    public String multiplicar(@WebParam(name = "name") int numero) {
        String retornar = "";
        for (int i = 1; i <= 10; i++) {
            retornar += numero+" * " + i +" = " + (numero*i)+"<br>";
        }
        return retornar;
    }
}
