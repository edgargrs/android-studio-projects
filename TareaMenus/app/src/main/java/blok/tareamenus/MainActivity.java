package blok.tareamenus;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Button btnOferta, btnNuestro, btnAdmisiones, btnSitio;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnOferta = (Button) findViewById(R.id.btn_oferta);
        btnNuestro = (Button) findViewById(R.id.btn_nuestro_campus);
        btnAdmisiones = (Button) findViewById(R.id.btn_admisiones);
        btnSitio = (Button) findViewById(R.id.btn_sitio_web);

        btnNuestro.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                nuestroCampus();
            }
        });
        btnSitio.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                sitioWeb();
            }
        });
        btnAdmisiones.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                admisiones();
            }
        });

        registerForContextMenu(btnOferta);

        btnOferta.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                PopupMenu menuPopUp = new PopupMenu(MainActivity.this, v);
                menuPopUp.setOnMenuItemClickListener(eventoPopUp);
                MenuInflater inflater=menuPopUp.getMenuInflater();
                inflater.inflate(R.menu.menu_oferta,menuPopUp.getMenu());
                menuPopUp.show();
            }
        } );

    }

    public void admisiones(){
        Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse("http://heila.utmorelia.edu.mx:8080/Heila/Faces"));
        startActivity(intent);
    }

    public void sitioWeb(){
        Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse("http://www.utmorelia.edu.mx/"));
        startActivity(intent);
    }

    public void nuestroCampus(){
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:19.726794,-101.16199979"));
        startActivity(intent);
        /* Intent i = new Intent(this, MainActivity.class);
        startActivity(i); */
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_mision_vision, menu);
        menu.add(menu.NONE, 100, menu.NONE, "Cerrar");
        return true;
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_oferta,menu);
        menu.add(menu.NONE,100,menu.NONE,"Cerrar");
    }

    private PopupMenu.OnMenuItemClickListener eventoPopUp=new PopupMenu.OnMenuItemClickListener() {
        public boolean onMenuItemClick(MenuItem item) {
            int id = item.getItemId();
            if (id == R.id.btn_tic) {
                tic();
            } else if (id == R.id.btn_bio) {
                bio();
            } else if (id == R.id.btn_mai) {
                mai();
            } else if (id == 100) {
                finish();
            }
            return false;
        }
    };

    public boolean onContextItemSelected(MenuItem item){
        int id=item.getItemId();
        if (id==R.id.btn_tic){
            tic();
        }else if(id==R.id.btn_bio){
            bio();
        }else if(id==R.id.btn_mai) {
            mai();
        }else if (id==100){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean tic(){
        Intent i = new Intent(this, TicsActivity.class);
        startActivity(i);
        return true;
    }

    public boolean bio(){
        Intent i = new Intent(this, BioActivity.class);
        startActivity(i);
        return true;
    }

    public boolean mai(){
        Intent i = new Intent(this, MaiActivity.class);
        startActivity(i);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if(id == R.id.btn_menu_mision){
            Intent i = new Intent(this, MisionActivity.class);
            startActivity(i);
            return true;
        }else if( id == R.id.btn_menu_vision){
            Intent i = new Intent(this, VisionActivity.class);
            startActivity(i);
            return true;
        }else if(id == 100){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
