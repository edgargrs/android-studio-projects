package com.example.blok.dialogos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button buttonDialogo, buttonDialogoOp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonDialogo = ( Button)findViewById(R.id.button_dialogo );
        clickDialogo();

        buttonDialogoOp = ( Button)findViewById(R.id.button_opciones );
        clickDialogoOp();
    }

    public void clickDialogo(){
        buttonDialogo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                DialogoUno d1=new DialogoUno();
                d1.show(getSupportFragmentManager(),"Mi dialogo");
            }
        });
    }

    public void clickDialogoOp(){ //Click del botón
        buttonDialogoOp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogoConElementos de = new DialogoConElementos();
                de.show(getFragmentManager(),"Opciones");
            }
        });
    }
}
