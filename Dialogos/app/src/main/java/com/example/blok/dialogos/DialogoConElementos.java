package com.example.blok.dialogos;

/**
 * Created by blok on 30/07/17.
 */

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.Toast;

public class DialogoConElementos extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.Titulo));//Agregamostitulo al dialogo
        builder.setMessage(R.string.Contenido);//Agregamoscontenido al dialogo
        builder.setIcon(R.mipmap.ic_launcher);//Agregamosicono al dialogo

        builder.setPositiveButton(R.string.Aceptar, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.e("Opción", "Presiono Aceptar");//Mensaje enviado a consola
                aceptar();
            }

        });

        builder.setNegativeButton(getString(R.string.Cancelar), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.e("Opción", "Presiono Cancelar"); //Mensaje enviado a la consola
                cancelar();
            }
        });
        return builder.create();
    }

    public void aceptar() { //Mètodo a ejecutar cuando se presione el botón de aceptar
        //Al aceptar iniciara una nueva actividad Ventana2 por lo que deben crearla
        Intent i=new Intent(getActivity(), Ventana2.class);
        startActivity(i);//Iniciala actividad
        Toast t=Toast.makeText(getActivity(), R.string.toastMensaje, Toast.LENGTH_SHORT);
        // Toast proporciona información al usuario en una pequeña ventana emergente (ventana activa, mensaje, duración)
        t.show(); //Muestra el mensaje
    }

    public void cancelar (){//Método a ejecutar cuando se presione el botón de cancelar
        getActivity().finish(); //Temina la aplicación
    }
}
