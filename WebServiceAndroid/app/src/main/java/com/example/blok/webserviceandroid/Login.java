package com.example.blok.webserviceandroid;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;


public class Login extends AppCompatActivity {

    static boolean errored = false;
    Button b;
    TextView statusTV;
    EditText userNameET , passWordET, edIp;
    ProgressBar webservicePG;
    String editTextUsername, ip;
    boolean loginStatus;
    String editTextPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Bundle bundle = getIntent().getExtras();
        //.---------Obtenemos los id de los campos de texto, botón, barra y de las etiquetas.
        // ------------------------------------------------------------------------------------
        userNameET = (EditText) findViewById(R.id.editText1);
        passWordET = (EditText) findViewById(R.id.editText2);
        statusTV = (TextView) findViewById(R.id.tv_result);
        //edIp = (EditText) findViewById(R.id.edIp);
        b = (Button) findViewById(R.id.button1);
        webservicePG = (ProgressBar) findViewById(R.id.progressBar1);
        ip = bundle.getString("ip");
        Log.e("Mensaje", "Ip recibida:" +ip);

        b.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                //Validamos el contenido de los campos de texto para evitar que pasen vacios.
                if (userNameET.getText().length() != 0 && userNameET.getText().toString() != "") {
                    if (passWordET.getText().length() != 0 && passWordET.getText().toString() != "") {
                        editTextUsername = userNameET.getText().toString();
                        editTextPassword = passWordET.getText().toString();
                        //ip = edIp.getText().toString();
                        statusTV.setText("");
                        //Creamos un objeto de la clase AsyncCallWS para poder llamar al método.
                        AsyncCallWS task = new AsyncCallWS();
                        //Lllamada al método de execute de la clase AsyncCallWS
                        task.execute();
                    }else{ //Si el campo de la clase es vacio....
                        statusTV.setText("Favor de ingresar su clave");
                    }//Si el campo del usuario es vacio....
                } else {
                    statusTV.setText("Favor de ingresar su usuario");
                }

            }
        });
    }

    private class AsyncCallWS extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... params) {
            //Lamamos al método CallLogin que definimos en la clase webservice, pasando como parametros los
            // campos de texto y el nombre del método que llamaremos del webService.
            loginStatus = WebService.LlamadaLogin(editTextUsername, editTextPassword, "login", ip);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) { //Ocultar la barra loading
            webservicePG.setVisibility(View.INVISIBLE);
            //creamos el Intent que llamara la siguiente activity, en este caso llamará a una activity llamada Menu
            Intent ActivitySiguiente = new Intent(Login.this, Welcome.class);
            //si ocurre un error
            if (!errored) {//validamos el resultado del método
                if (loginStatus) {
                    //Si el resultado del metodo "LlamadaLogin" es verdadero, se cargará la activity definida en la variable
                    // ActivitySiguiente
                    startActivity(ActivitySiguiente);
                } else {
                    //Si el resultado del método es falso quiere decir que los datos del usuario son incorrectos.
                    statusTV.setText("DATOS INCORRECTOS");
                }
                //Si la falla esta en la llamada al nuestro WebService:
            } else {
                statusTV.setText("Hay un error al llamar al Ws :(");
            }
            //La variable errored de nuevo la ponemos en false para indicar la falla
            errored = false;
        }

        @Override
        //Mostramos la barra de loading mientras recibimos una respuesta.
        protected void onPreExecute() {webservicePG.setVisibility(View.VISIBLE);}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }


}
