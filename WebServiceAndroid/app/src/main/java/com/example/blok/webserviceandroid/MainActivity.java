package com.example.blok.webserviceandroid;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.AsyncTask;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button b, login; TextView tv; EditText et, edIp; ProgressBar pg; String editText, ip; String displayText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Asignamos a la variable et el id del campo de texto.
        et = (EditText) findViewById(R.id.editText1);
        //Asignamos el id del textView a la variable tv
        tv = (TextView) findViewById(R.id.tv_result);
        //La variable b tomará el id de l botón creado.
        b = (Button) findViewById(R.id.button1);
        edIp = (EditText) findViewById(R.id.edIp);
        login = (Button) findViewById(R.id.btnLogin);
        //a la variable pg le asignamos el valor de nuestra view progressBar1
        pg = (ProgressBar) findViewById(R.id.progressBar1);
        //iniciamos las funciones de nuestro botón.
        b.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                //VALIDAMOS QUE EL USUARIO INTRODUZCA AL MENOS UN DATO EN EL CAMPO DE TEXTO
                if (et.getText().length() != 0 && et.getText().toString() != "") {
                    //Obtenemos con la función getText el valor del campo de texto.
                    editText = et.getText().toString();
                    ip = edIp.getText().toString();
                    //Creamos un objeto de la clase AsyncCallWS para la ejecución del WS
                    AsyncCallWS task = new AsyncCallWS();
                    //LLamamos al método de ejecución
                    task.execute();
                } else { //Si el campo de texto esta vacio enviamos un mensaje al usuario
                    tv.setText("Por favor introduzca un dato");
                }
            }
        });

        login.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                cambia();
            }
        });
    }

    public void cambia(){
        ip = edIp.getText().toString();
        Intent i = new Intent(this, Login.class);
        i.putExtra("ip",ip);
        startActivity(i);
    }

    private class AsyncCallWS extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... params) {
            //Lamamos al método "LlamadaMetodo" que definimos en la clase webservice, pasando como parametros
            //los campos de texto
            // y el nombre del método que llamaremos del webService.
            Log.e("Enviado a webservice", "Numero: " + editText); //Mensaje enviado a la consola
            displayText = WebService.LlamadaMetodo(editText, "tabla", ip); //especificar nombre del método
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            tv.setText(displayText);
            pg.setVisibility(View.INVISIBLE);
        }

        @Override
        protected void onPreExecute() {
            pg.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }
}
