package com.example.blok.webserviceandroid;

/**
 * Created by blok on 1/08/17.
 */

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class WebService {

    //Namespace de nuestro Web Service
    private static String NAMESPACE = "http://sw/";
    //Webservice URL
    private static String URL;
    //SOAP Action URI Namespace + Web method name
    private static String SOAP_ACTION = "http://sw/";



    public static boolean LlamadaLogin(String userName,String passWord, String webMethName, String ip) {
        // Por default dejamos a la variable, que esperamos como resultado de este método, como false
        boolean loginStatus = false;
        // creamos la solicitud de conexion al web service
        SoapObject request = new SoapObject(NAMESPACE, webMethName);
        // Creamos 2 variables de tipo PropetyInfo para lo parámetros que requerimos en el método
        // de login en el WebService
        PropertyInfo parametroUsr = new PropertyInfo();
        PropertyInfo parametroClave = new PropertyInfo();
//------Parametro Usuario
        parametroUsr.setName("usuario"); // Asignamos a la primer variable el nombre del primer parámetro que
        // espera el WebService.
        parametroUsr.setValue(userName); //Asignamos a esta variable el valor que pasemos como parametro en la
        // llamada a éste método
        parametroUsr.setType(String.class);//Asignamos el tipo de dato de la variable
        request.addProperty(parametroUsr); //Hacemos el request de esta variable.
//------Parametro clave (password)
        parametroClave.setName("clave");
        parametroClave.setValue(passWord);
        parametroClave.setType(String.class);
        request.addProperty(parametroClave);

        // Creación del envio del ws a traves de una instancia a la clase SoapSerializationEnvelope.
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        envelope.setOutputSoapObject(request);

        URL = "http://"+ip+":8080/ServidorTabla/Servicios?WSDL";
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        try { //Iniciamos la excepcion para oder enviar la petición al Web Service.
            androidHttpTransport.call(SOAP_ACTION+webMethName, envelope);
            //recibimos la respuesta del WebService en la variable loginStatus
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            loginStatus = Boolean.parseBoolean(response.toString());
        } catch (Exception e) {
            //Si existe algun error en el envio, la variable errored que se creo en el metodo LlamadaLogin sera verdadera
            Login.errored = true;
            e.printStackTrace();
        }
        //Regresamos como resultado de este método la variable loginStatus.
        return loginStatus;
    }




    ///  METODO PARA LLAMAR A MÉTODO con un parámetro
    ///  en el WEBSERVICE
    public static String LlamadaMetodo(String name, String webMethName, String ip) {
        String resTxt = null;
        // Creamos la solicitud de conexion al web service
        SoapObject request = new SoapObject(NAMESPACE, webMethName);
        // Property almacenara los parámetros requeridos por el método en el webService
        PropertyInfo parametro1 = new PropertyInfo();
        // n es el nombre del parametro que espera el webService.
        parametro1.setName("name");
        // Asignamos el valor a parametro
        parametro1.setValue(name);
        parametro1.setType(String.class);
        request.addProperty(parametro1);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.setOutputSoapObject(request);
        // llamada a la url por el protocolo http
        URL = "http://"+ip+":8080/ServidorTabla/Servicios?WSDL";
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        try {
            // llamada al WS
            androidHttpTransport.call(SOAP_ACTION+webMethName, envelope);
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            resTxt = response.toString();
        } catch (Exception e) {
            //En caso de que no responda el Ws
            e.printStackTrace();
            resTxt = "Hay un error al llamar al Ws :(";
        }
        //Return resTxt
        return resTxt;
    }
}
