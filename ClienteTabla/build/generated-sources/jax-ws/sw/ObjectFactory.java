
package sw;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the sw package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TablaResponse_QNAME = new QName("http://sw/", "tablaResponse");
    private final static QName _Tabla_QNAME = new QName("http://sw/", "tabla");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: sw
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TablaResponse }
     * 
     */
    public TablaResponse createTablaResponse() {
        return new TablaResponse();
    }

    /**
     * Create an instance of {@link Tabla }
     * 
     */
    public Tabla createTabla() {
        return new Tabla();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TablaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sw/", name = "tablaResponse")
    public JAXBElement<TablaResponse> createTablaResponse(TablaResponse value) {
        return new JAXBElement<TablaResponse>(_TablaResponse_QNAME, TablaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tabla }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sw/", name = "tabla")
    public JAXBElement<Tabla> createTabla(Tabla value) {
        return new JAXBElement<Tabla>(_Tabla_QNAME, Tabla.class, null, value);
    }

}
