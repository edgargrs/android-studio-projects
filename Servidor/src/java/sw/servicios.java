/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sw;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Alumno
 */
@WebService(serviceName = "servicios")
public class servicios {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "suma")
    public String suma(@WebParam(name = "n1") int num1,@WebParam(name = "n2") int num2 ) {
        return "La suma es: " + (num1 + num2);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "resta")
    public String resta(@WebParam(name = "num1") int num1, @WebParam(name = "num2") int num2) {
        //TODO write your implementation code here:
        return null;
    }
}
