<%-- 
    Document   : Consumir
    Created on : 18/07/2017, 10:54:10 AM
    Author     : Alumno
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="Consumir.jsp" method="post">
            Número 1: <input type="text" name="numero1"/>
            Número 2: <input type="text" name="numero2"/>
            <input type="submit" value="Sumar"/>
        </form>
            <%-- start web service invocation --%><hr/>
    <%
    try {
	sw.Servicios_Service service = new sw.Servicios_Service();
	sw.Servicios port = service.getServiciosPort();
	 // TODO initialize WS operation arguments here
	int n1 = Integer.parseInt(request.getParameter("numero1"));
	int n2 = Integer.parseInt(request.getParameter("numero2"));
	// TODO process result here
	java.lang.String result = port.suma(n1, n2);
	out.println("Resultado = "+result);
    } catch (Exception ex) {
	// TODO handle custom exceptions here
    }
    %>
    <%-- end web service invocation --%><hr/>

    </body>
</html>
